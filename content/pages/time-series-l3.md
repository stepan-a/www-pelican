Title: Économétrie des séries temporelles
Authors: Stéphane Adjemian
URL: teaching/time-series
save_as: teaching/time-series/index.html
status: hidden

# Bibliographie
 - *Time Series Analysis*, James, D. Hamilton, Princeton University Press, 1994.
 - *Séries temporelles et modèles dynamiques*,  Christian Gourieroux et Alain Monfort, Economica, 1995.

<br>

# Travaux dirigés
 - Fiche de TD n°1 : [pdf](../../university/time-series/td/1/td.pdf), [tex](../../university/time-series/td/1/td.tex)
 - Fiche de TD n°2 : [pdf](../../university/time-series/td/2/td.pdf), [tex](../../university/time-series/td/2/td.tex)

# Partiels
 - 2012-2013
   Le [sujet](http://perso.univ-lemans.fr/~sadjem/cours/series-temporelles/2012-2013/partiel.pdf) et sa [correction](http://perso.univ-lemans.fr/~sadjem/cours/series-temporelles/2012-2013/correction-partiel.pdf)
